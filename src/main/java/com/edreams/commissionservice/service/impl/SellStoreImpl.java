package com.edreams.commissionservice.service.impl;

import com.edreams.commissionservice.sale.SaleType;
import com.edreams.commissionservice.service.SellStore;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

//This class is just a declaration so that project can be build and possible test cases can be run

@Service
public class SellStoreImpl implements SellStore {
    @Override
    public void saveCommission(SaleType sell, Double commission) {

    }

    @Override
    public List<SaleType> getSells(Date date) {
        return null;
    }

    @Override
    public List<SaleType> getSells(Date fromDate, Date toDate, String sellerId) {
        return null;
    }
}
