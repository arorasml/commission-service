package com.edreams.commissionservice.service.impl;

import com.edreams.commissionservice.sale.SaleType;
import com.edreams.commissionservice.service.SaleScreenPrinter;
import org.springframework.stereotype.Service;

//This class is just a declaration so that project can be build and possible test cases can be run

@Service
public class SaleScreenPrinterImpl implements SaleScreenPrinter {
    @Override
    public void printSell(SaleType sell) {

    }
}
