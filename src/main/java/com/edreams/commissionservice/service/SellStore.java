package com.edreams.commissionservice.service;

import com.edreams.commissionservice.sale.SaleType;

import java.util.Date;
import java.util.List;

public interface SellStore {

    //save commission earned by seller to db. SaleType object will have sellerId
    void saveCommission(SaleType sell,Double commission);
    // return all the sells of 1 day
    List<SaleType> getSells(Date date);

    //return the sells of a seller in a range of date
    List<SaleType> getSells(Date fromDate, Date toDate, String sellerId);
}
