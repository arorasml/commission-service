package com.edreams.commissionservice.service;

import com.edreams.commissionservice.calculation.CommissionCalculationContextFactory;
import com.edreams.commissionservice.sale.SaleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesService {

    @Autowired
    private EmailSender emailSender;
    @Autowired
    private SellStore sellStore;
    @Autowired
    private SaleScreenPrinter screenPrinter;
    @Autowired
    private CommissionCalculationContextFactory commissionCalculationContextFactory;

    public void sellProductType(SaleType sell)
    {
        // calculate commission
        Double commissonEarned = commissionCalculationContextFactory.getContext(sell).executeCalculations(sell);

        //sellStore will save commission for the seller
        sellStore.saveCommission(sell,commissonEarned);

        //emailSender will send notification to client
        emailSender.sendMail(sell);

        //screenPrinter will print results on office screen
        screenPrinter.printSell(sell);
    }

}
