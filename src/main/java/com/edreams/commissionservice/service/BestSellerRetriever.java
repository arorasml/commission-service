package com.edreams.commissionservice.service;

import com.edreams.commissionservice.repository.CommissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/*The company each day gives the “best seller of the day”
        prize to the seller that closes more
        sales.*/
@Service
public class BestSellerRetriever {

    @Autowired
    private CommissionRepository commissionRepository;

    public String getBestSellerId(Date date)
    {
        return commissionRepository.getBestSeller(date);
    }
}
