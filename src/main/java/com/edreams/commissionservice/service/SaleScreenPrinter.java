package com.edreams.commissionservice.service;

import com.edreams.commissionservice.sale.SaleType;

public interface SaleScreenPrinter {

    //Print it on the commons office screen
    void printSell(SaleType sell);
}
