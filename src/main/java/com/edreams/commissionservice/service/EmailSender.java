package com.edreams.commissionservice.service;

import com.edreams.commissionservice.sale.SaleType;

public interface EmailSender {

    // SaleType object have client info embedded.
    void sendMail(SaleType sell);
}
