package com.edreams.commissionservice.service;

import com.edreams.commissionservice.repository.CommissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CommisionCalculator {

    @Autowired
    private CommissionRepository commissionRepository;

    public double getSellerCommision(Date fromDate, Date toDate, String sellerId)
    {
        return commissionRepository.getSellerCommission(fromDate,toDate,sellerId);
    }
}
