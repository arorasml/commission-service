package com.edreams.commissionservice.controller;

import com.edreams.commissionservice.sale.SaleTypeA;
import com.edreams.commissionservice.sale.SaleTypeB;
import com.edreams.commissionservice.sale.SaleTypeC;
import com.edreams.commissionservice.service.BestSellerRetriever;
import com.edreams.commissionservice.service.CommisionCalculator;
import com.edreams.commissionservice.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/commission")
public class CommissionServiceController {

    @Autowired
    private SalesService salesService;

    @Autowired
    private CommisionCalculator commisionCalculator;

    @Autowired
    private BestSellerRetriever bestSellerRetriever;

    @PutMapping("/sellTypeA")
    public void sellTypeA(@RequestBody SaleTypeA sale)
    {
        salesService.sellProductType(sale);
    }
    @PutMapping("/sellTypeB")
    public void sellTypeB(@RequestBody SaleTypeB sale){
        salesService.sellProductType(sale);
    }
    @PutMapping("/sellTypeC")
    public void sellTypeC(@RequestBody SaleTypeC sale){
        salesService.sellProductType(sale);
    }

    @GetMapping("/bestSeller")
    public String getBestSellerId(@RequestParam Date date)
    {
        return bestSellerRetriever.getBestSellerId(date);
    }

    @GetMapping("/sellerCommission")
    public double getSellerCommission(@RequestParam Date fromDate,@RequestParam Date toDate,
                                      @RequestParam String sellerId)
    {
        return commisionCalculator.getSellerCommision(fromDate,toDate,sellerId);
    }
}
