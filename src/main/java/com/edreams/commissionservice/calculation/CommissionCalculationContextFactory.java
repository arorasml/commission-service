package com.edreams.commissionservice.calculation;

import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeA;
import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeB;
import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeC;
import com.edreams.commissionservice.sale.SaleType;
import com.edreams.commissionservice.sale.SaleTypeA;
import com.edreams.commissionservice.sale.SaleTypeB;
import com.edreams.commissionservice.sale.SaleTypeC;
import org.springframework.stereotype.Service;

/*This implementation is based on
factory design pattern, where
calculation context is created
on the basis of SaleType.*/
@Service
public class CommissionCalculationContextFactory {

    //get context on the basis of saleType

    public ComissionCalculationContext getContext(SaleType saleType)
    {
        ComissionCalculationContext context = new ComissionCalculationContext();


        if(saleType instanceof SaleTypeA)
        {
            context.setCalcStrategy(new CommissionCalcStrategyTypeA());
        }

        else if(saleType instanceof SaleTypeB)
        {
            context.setCalcStrategy(new CommissionCalcStrategyTypeB());
        }

        else if(saleType instanceof SaleTypeC)
        {
            context.setCalcStrategy(new CommissionCalcStrategyTypeC());
        }

        return context;
    }
}
