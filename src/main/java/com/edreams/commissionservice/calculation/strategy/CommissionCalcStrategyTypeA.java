package com.edreams.commissionservice.calculation.strategy;

import com.edreams.commissionservice.sale.SaleTypeA;

// TypeA: the seller earns a commission of 1% of the product price.
public class CommissionCalcStrategyTypeA implements CommissionCalcStrategy<SaleTypeA>{
    @Override
    public double calulateCommission(SaleTypeA sale) {
        return (sale.getPrice()*.01);
    }
}
