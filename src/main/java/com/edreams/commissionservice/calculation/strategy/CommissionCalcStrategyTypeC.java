package com.edreams.commissionservice.calculation.strategy;

import com.edreams.commissionservice.sale.SaleTypeC;

//TypeC: the seller has a fixed commission of 5 euros.
public class CommissionCalcStrategyTypeC implements CommissionCalcStrategy<SaleTypeC>{
    @Override
    public double calulateCommission(SaleTypeC sale) {
        return 5.0;
    }
}
