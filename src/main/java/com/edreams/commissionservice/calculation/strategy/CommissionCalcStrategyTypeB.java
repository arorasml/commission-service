package com.edreams.commissionservice.calculation.strategy;

import com.edreams.commissionservice.sale.SaleTypeB;

/*TypeB: the product has an attribute number of pieces and the commission is
        calculated as 1 euro for peace.*/
public class CommissionCalcStrategyTypeB implements CommissionCalcStrategy<SaleTypeB>{
    @Override
    public double calulateCommission(SaleTypeB sale) {
        return sale.getUnitSold()*1.0;
    }
}
