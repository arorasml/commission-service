package com.edreams.commissionservice.calculation.strategy;


/*This implementation is based
on strategy design pattern,
for new SaleType, new Strategy can
be added and hence need not to change existing
saleType classes.*/

public interface CommissionCalcStrategy<SaleType> {

    double calulateCommission(SaleType sale);
}
