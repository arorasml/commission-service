package com.edreams.commissionservice.calculation;

import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategy;
import com.edreams.commissionservice.sale.SaleType;
import org.springframework.stereotype.Service;

@Service
public class ComissionCalculationContext {
    private CommissionCalcStrategy calcStrategy;

    public ComissionCalculationContext() {
    }

    public void setCalcStrategy(CommissionCalcStrategy calcStrategy) {
        this.calcStrategy = calcStrategy;
    }

    public double executeCalculations(SaleType saleType)
    {
        return calcStrategy.calulateCommission(saleType);
    }
}
