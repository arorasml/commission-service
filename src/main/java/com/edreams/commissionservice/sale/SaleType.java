package com.edreams.commissionservice.sale;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class SaleType {

    private double price;
    private String sellerId;
    private String clientEmail;

}
