package com.edreams.commissionservice.sale;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaleTypeB extends SaleType{

    private int unitSold;

}
