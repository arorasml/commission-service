package com.edreams.commissionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommissionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommissionServiceApplication.class, args);
	}

}
