package com.edreams.commissionservice.testdata;

import com.edreams.commissionservice.sale.SaleType;
import com.edreams.commissionservice.sale.SaleTypeA;
import com.edreams.commissionservice.sale.SaleTypeB;
import com.edreams.commissionservice.sale.SaleTypeC;

public class SalesTestData {

    public static SaleTypeA createSaleTypeAData()
    {
        SaleTypeA saleTypeA = new SaleTypeA();
        saleTypeA.setClientEmail("abc@xyz.com");
        saleTypeA.setPrice(5);
        saleTypeA.setSellerId("Sender-1");
        return saleTypeA;
    }

    public static SaleTypeB createSaleTypeBData()
    {
        SaleTypeB saleTypeB = new SaleTypeB();
        saleTypeB.setClientEmail("abc@xyz.com");
        saleTypeB.setPrice(5);
        saleTypeB.setSellerId("Sender-2");
        saleTypeB.setUnitSold(10);
        return saleTypeB;
    }

    public static SaleTypeC createSaleTypeCData()
    {
        SaleTypeC saleTypeC = new SaleTypeC();
        saleTypeC.setClientEmail("abc@xyz.com");
        saleTypeC.setPrice(5);
        saleTypeC.setSellerId("Sender-3");
        return saleTypeC;
    }
}
