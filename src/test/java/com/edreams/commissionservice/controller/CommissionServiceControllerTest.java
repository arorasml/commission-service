package com.edreams.commissionservice.controller;

import com.edreams.commissionservice.CommissionServiceApplication;
import com.edreams.commissionservice.sale.SaleTypeA;
import com.edreams.commissionservice.sale.SaleTypeB;
import com.edreams.commissionservice.sale.SaleTypeC;
import com.edreams.commissionservice.testdata.SalesTestData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CommissionServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CommissionServiceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void contextLoads() {
    }

    //sellTypeA product
    @Test
    public void testsellTypeA() {
        HttpHeaders headers = new HttpHeaders();
        SaleTypeA testObj = SalesTestData.createSaleTypeAData();
        HttpEntity entity = new HttpEntity<>(testObj, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/commission/sellTypeA", HttpMethod.PUT, entity,
                String.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
    }

    //sellTypeB product
    @Test
    public void testsellTypeB() {
        HttpHeaders headers = new HttpHeaders();
        SaleTypeB testObj = SalesTestData.createSaleTypeBData();
        HttpEntity entity = new HttpEntity<>(testObj, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/commission/sellTypeB", HttpMethod.PUT, entity,
                String.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
    }

    //sellTypeC product
    @Test
    public void testsellTypeC() {
        HttpHeaders headers = new HttpHeaders();
        SaleTypeC testObj = SalesTestData.createSaleTypeCData();
        HttpEntity entity = new HttpEntity<>(testObj, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/commission/sellTypeC", HttpMethod.PUT, entity,
                String.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
    }

}
