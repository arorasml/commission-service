package com.edreams.commissionservice.strategy;

import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeB;
import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeC;
import com.edreams.commissionservice.sale.SaleTypeB;
import com.edreams.commissionservice.sale.SaleTypeC;
import com.edreams.commissionservice.testdata.SalesTestData;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommissionCalcStrategyTypeCTest {

    CommissionCalcStrategyTypeC strategyTypeC;

    @Before
    public void setup()
    {
        strategyTypeC = new CommissionCalcStrategyTypeC();
    }

    @Test
    public void calulateCommissionTest()
    {
        SaleTypeC saleTypeC = SalesTestData.createSaleTypeCData();
        double res = strategyTypeC.calulateCommission(saleTypeC);
        assertEquals(5.0,res,0);
    }
}
