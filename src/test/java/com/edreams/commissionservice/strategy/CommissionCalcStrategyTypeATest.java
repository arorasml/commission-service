package com.edreams.commissionservice.strategy;


import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeA;
import com.edreams.commissionservice.sale.SaleTypeA;
import com.edreams.commissionservice.testdata.SalesTestData;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CommissionCalcStrategyTypeATest {

    CommissionCalcStrategyTypeA strategyTypeA;

    @Before
    public void setup()
    {
        strategyTypeA = new CommissionCalcStrategyTypeA();
    }

    @Test
    public void calulateCommissionTest()
    {
        SaleTypeA saleTypeA = SalesTestData.createSaleTypeAData();
        double res = strategyTypeA.calulateCommission(saleTypeA);
        assertEquals(.05,res,0);
    }

}
