package com.edreams.commissionservice.strategy;

import com.edreams.commissionservice.calculation.strategy.CommissionCalcStrategyTypeB;
import com.edreams.commissionservice.sale.SaleTypeB;
import com.edreams.commissionservice.testdata.SalesTestData;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommissionCalcStrategyTypeBTest {

    CommissionCalcStrategyTypeB strategyTypeB;

    @Before
    public void setup()
    {
        strategyTypeB = new CommissionCalcStrategyTypeB();
    }

    @Test
    public void calulateCommissionTest()
    {
        SaleTypeB saleTypeB = SalesTestData.createSaleTypeBData();
        double res = strategyTypeB.calulateCommission(saleTypeB);
        assertEquals(10.0,res,0);
    }

}
